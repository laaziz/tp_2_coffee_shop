//
//  ContentView.swift
//  Coffee Shope
//
//  Created by piman on 4/23/20.
//  Copyright © 2020 piman. All rights reserved.
//

import SwiftUI
import Firebase

struct ContentView: View {
    
    init() {
        UINavigationBar.appearance().backgroundColor = UIColor(red: 0.36, green: 0.26, blue: 0.20, alpha: 1.00)
        UINavigationBar.appearance().barTintColor = UIColor(red: 0.36, green: 0.26, blue: 0.20, alpha: 1.00)
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor:UIColor.white]
    }
    
    @ObservedObject var drinkListener = DrinkListener()
    @State private var showingBasket = false
    var categories : [String  : [Drink]] {
        .init(
            grouping : drinkListener.drinks ,
            by : {$0.category.rawValue}
        )
    }
    
    
    var body: some View {
        NavigationView {
            List(categories.keys.sorted() , id : \String.self) {
                key in
                
                DrinkRow(categoryName: "\(key) Drink".uppercased(), drinks: self.categories[key]!)
            }
            .navigationBarTitle(
                Text("Coffee Shop").foregroundColor(.white), displayMode: .inline
            )
                .navigationBarItems(
                    leading:
                    Button(action :{
                        FUser.logOutCurrenUser { (error) in
                            print("error loging out user, ", error?.localizedDescription)
                        }
                        
                    } , label :{
                        Image(systemName: "power")
                            .foregroundColor(.white)
                    }
                    )
                    , trailing:
                    Button(action :{
                        self.showingBasket.toggle()
                        print("basket")
                    } , label :{
                        Image(systemName: "cart")
                            .foregroundColor(.white)
                    }
                    )
                        .sheet(isPresented: $showingBasket){
                            if FUser.currentUser() != nil && FUser.currentUser()!.onBoarding {
                                
                                OrderBasketView()
                                
                            } else {
                                LoginView()
                            }
                            
                    }
            )
        }
        
    }
    
    
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
