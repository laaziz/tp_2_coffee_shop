import Foundation
import Firebase
 
class BasketListener : ObservableObject{
    @Published var orderBasket : OrderBasket!
    init(){
        downloadBasket()
    }
    func downloadBasket(){
        FirebaseReference(.Basket).whereField(KOWNERID, isEqualTo: FUser.currentId()).addSnapshotListener{
            (snapshot , error) in
            guard let snapshot = snapshot else { return }
            if !snapshot.isEmpty  {
                
                let basketData = snapshot.documents.first!.data()
                
                getDrinksFromFirestore(withIds: basketData[KDRINKIDS] as?  [String] ??  []){
                    (allDrink) in
                    let basket = OrderBasket()
                    basket.ownerId = basketData[KOWNERID] as? String
                    basket.id = basketData[KID] as? String
                    basket.items = allDrink
                 
                    self.orderBasket = basket
                    
                  
                }
            }
        }
        
    }
}
func getDrinksFromFirestore(withIds : [String] , completion : @escaping ( _ drinkArray : [Drink]) -> Void ){
    var count  = 0
    var drinkArray : [Drink] = []
    if withIds.count == 0{
        completion(drinkArray)
        return
    }
    for drinkId in withIds{
        FirebaseReference(.Menu).whereField(KID , isEqualTo: drinkId).getDocuments{
            (snapshot , error ) in
            guard let snapshot = snapshot else { return }
            if !snapshot.isEmpty {
                let drinkData = snapshot.documents.first!
                let drinkItem  = Drink(
                    id : drinkData[KID] as? String ?? UUID().uuidString,
                          name : drinkData[KNAME] as? String ?? "UNKNOWN" ,
                          imageName :drinkData[KIMAGENAME] as? String ?? "UNKNOWN" ,
                          category : Category(rawValue:
                           drinkData[KCATEGORY] as? String ?? "cold") ?? .cold ,
                          
                          description :drinkData[KDESCRIPTION] as? String ?? "UNKNOWN" ,
                           price : drinkData[KPRICE] as? Double ?? 0.0
                           )
                drinkArray.append(drinkItem)
                count += 1
            } else {
                print("have no drink")
                completion(drinkArray)
            }
            
            if count == withIds.count{
                completion(drinkArray)
            }
        }
    }
}
