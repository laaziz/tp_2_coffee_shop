//
//  Extensions.swift
//  Coffee Shope
//
//  Created by piman on 4/25/20.
//  Copyright © 2020 piman. All rights reserved.
//

import Foundation

extension Double {
    var format : String {
        return String(format : "%.2f" , self)
    }
}
