//
//  DrinkListener.swift
//  Coffee Shope
//
//  Created by piman on 4/25/20.
//  Copyright © 2020 piman. All rights reserved.
//

import Foundation
import Firebase
 
class DrinkListener: ObservableObject {
 
    @Published var drinks : [Drink] = []

init(){
      downloadDrinks()
  }
  func downloadDrinks(){
      FirebaseReference(.Menu).getDocuments { (snapshot ,error) in
          guard let snapshot = snapshot else { return }
          if !snapshot.isEmpty{
              self.drinks = DrinkListener.drinkFromDictionnary(snapshot)
          }
          
      }
      
  }

    static func drinkFromDictionnary( _ snapshot : QuerySnapshot) -> [Drink]
    {
        var  allDrinks : [Drink] = []
        for snapshot in snapshot.documents {
            let drinkData =  snapshot.data()
            let drinkItem  = Drink(
           id : drinkData[KID] as? String ?? UUID().uuidString,
           name : drinkData[KNAME] as? String ?? "UNKNOWN" ,
           imageName :drinkData[KIMAGENAME] as? String ?? "UNKNOWN" ,
           category : Category(rawValue:
            drinkData[KCATEGORY] as? String ?? "cold") ?? .cold ,
           
           description :drinkData[KDESCRIPTION] as? String ?? "UNKNOWN" ,
            price : drinkData[KPRICE] as? Double ?? 0.0
            )
                allDrinks.append(drinkItem)
        }
        return allDrinks
    }
}

