 import SwiftUI
 
 struct OrderBasketView: View {
    @ObservedObject var basketListener = BasketListener()
    var body: some View {
        NavigationView {
            List{
                Section{
                    ForEach(self.basketListener.orderBasket?.items ?? []){ drink in
                        HStack{
                            Text(drink.name)
                            Spacer()
                            Text("$ \(drink.price)")
                            
                        }
                        
                        
                    }
                    .onDelete{ (indexSet) in
                        self.deleteItems(at : indexSet)
                        
                    }
                    Section{
                        
                        Text("Place order")
                        
                        
                    }.disabled(self.basketListener.orderBasket?.items.isEmpty ?? true)
                }
                Section{
                    NavigationLink(destination: CheckoutView()){
                        Text("Place order")
                    }
                }.disabled(self.basketListener.orderBasket?.items.isEmpty ?? true)
                
            }
            .navigationBarTitle(Text("Order").foregroundColor(.white))
            
            .listStyle(GroupedListStyle())
        }
    }
    func deleteItems(at offsets : IndexSet){
        self.basketListener.orderBasket.items.remove(at : offsets.first!)
        self.basketListener.orderBasket.saveBasketToFirestore()
    }
 }
 
 struct OrderBasketView_Previews: PreviewProvider {
    static var previews: some View {
        OrderBasketView()
    }
 }
