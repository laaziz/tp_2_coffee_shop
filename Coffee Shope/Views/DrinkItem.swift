//
//  DrinkItem.swift
//  Coffee Shope
//
//  Created by piman on 4/25/20.
//  Copyright © 2020 piman. All rights reserved.
//

import SwiftUI
 
struct DrinkItem: View {
    var drink : Drink
    var body: some View {
       VStack(alignment: .leading , spacing:  16){
             Image(self.drink.imageName)
                .resizable()
                .renderingMode(.original)
                .aspectRatio(contentMode : .fill)
                .frame(width : 300 , height: 170)
                .cornerRadius(10)
                .shadow(radius: 10)
            
            VStack(alignment: .leading , spacing:  5){
                Text(self.drink.name)
                    .foregroundColor(.init(UIColor(red: 0.36, green: 0.26, blue: 0.20, alpha: 1.00)))
                    .font(.headline)
                Text(self.drink.description)
                    .font(.subheadline)
                    .foregroundColor(.secondary)
                    .multilineTextAlignment(.leading)
                    .lineLimit(2)
                    .frame(height:40)
           
        } }
    }
}
 
struct DrinkItem_Previews: PreviewProvider {
    static var previews: some View {
        DrinkItem(drink: drinkData[0])
    }
}

