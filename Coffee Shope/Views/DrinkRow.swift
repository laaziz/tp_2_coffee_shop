//
//  DrinkRow.swift
//  Coffee Shope
//
//  Created by piman on 4/25/20.
//  Copyright © 2020 piman. All rights reserved.
//

import SwiftUI
 
struct DrinkRow: View {
    var categoryName : String
    var drinks : [Drink]
    
    var body: some View {
        VStack(alignment : .leading  ){
            HStack {
            Text(self.categoryName)
                .font(.system(size: 13))
                .foregroundColor(.white)
                .bold()
                .padding(.horizontal, 4)
                .padding(.vertical, 4)
               
            } .background(Color.init(UIColor(red: 0.36, green: 0.26, blue: 0.20, alpha: 1.00)))
                .cornerRadius(10).padding(.top, 5)
            ScrollView(.horizontal , showsIndicators: false){
                HStack{
                   
                    ForEach(self.drinks) {
                        drink in
                       
                            NavigationLink(destination: DrinkDetail(drink : drink)) {
                                                   DrinkItem(drink: drink)
                                                       .frame(width:300)
                                                       .padding(.trailing , 30)
                                                  }

                        
                    }
                    
                }
            }
        }
    }
}


 
struct DrinkRow_Previews: PreviewProvider {
    static var previews: some View {
        DrinkRow(categoryName:  "HOT DRINKS", drinks : drinkData  )
    }
}

