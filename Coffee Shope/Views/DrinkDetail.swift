import SwiftUI

struct DrinkDetail: View {
    
     @Environment(\.presentationMode) var presentationMode
    @State private var showingAlert = false
    var drink : Drink
    
    
    var body: some View {
        ScrollView(.vertical , showsIndicators: false) {
            ZStack(alignment: .top){
                Image(drink.imageName)
                    .resizable()
                    .aspectRatio(contentMode: .fill)
                Rectangle()
                    .frame(height:80)
                    .foregroundColor(.black)
                    .opacity(0.3)
                    .blur(radius: 10)
                HStack{
                    VStack(alignment: .leading , spacing: 8){
                        Button(action: {self.presentationMode.wrappedValue.dismiss()}) {
                        Image(systemName: "chevron.left").foregroundColor(.white)
                        }.font(.system(size: 20))
                    }
                    .padding(.leading)
                    .padding(.top, 50)
                    Spacer()
                }
                
            }
            .listRowInsets(EdgeInsets())
            Text(drink.name)
                                       .foregroundColor(.white )
                                       .font(.largeTitle)
            Text(drink.description)
                .foregroundColor(.primary)
                .font(.body)
                .lineLimit(5)
                .padding()
            HStack{
                Spacer()
                OrderButton(showAlert: $showingAlert, drink: self.drink)
                Spacer()
            }
            
            
        }
        .edgesIgnoringSafeArea(.all)
        .navigationBarTitle("")
        .navigationBarHidden(true)
        .navigationBarBackButtonHidden(false)
        .alert(isPresented: $showingAlert){
            Alert(title: Text("Add to basket"), dismissButton : .default(Text("OK")))
        }
    }
}

struct DrinkDetail_Previews: PreviewProvider {
    static var previews: some View {
        DrinkDetail(drink : drinkData.first!)
    }
}
struct OrderButton: View {
    @ObservedObject var basketListener =  BasketListener()
    @Binding var showAlert : Bool
    
    var drink : Drink
    
    private func addItemTobasket(){
        var orderBasket : OrderBasket
        
        if self.basketListener.orderBasket  != nil {
            orderBasket =  self.basketListener.orderBasket
        } else {
            orderBasket = OrderBasket()
            orderBasket.ownerId = FUser.currentId()
            orderBasket.id = UUID().uuidString
        }
        orderBasket.add(self.drink)
        orderBasket.saveBasketToFirestore()
    }
    
    
    var body: some View {
        Button(action:{
            self.showAlert.toggle()
            self.addItemTobasket()
            print("Add to basket  \(self.drink.name)" )
        }) {
            Text("Add to basket")
                .frame(width:200 , height: 50)
                .foregroundColor(.white)
                .font(.headline)
                .background(Color.init(UIColor(red: 0.36, green: 0.26, blue: 0.20, alpha: 1.00)))
                .cornerRadius(50)
            
            
        }
    }
    
}

