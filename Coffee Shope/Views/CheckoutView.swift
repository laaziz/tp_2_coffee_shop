import SwiftUI
 
struct CheckoutView: View {
    
    @ObservedObject var basketListener  = BasketListener()
    static let paymentTypes = ["Cash" ,  "Credit Card"]
    static let tipAmonts =  [10 , 15 , 20 , 0]
    
    @State private var payementType = 0
    @State private var tipAmont = 1
    @State private var showingAlert = false
    var totalPrice : Double {
        let total = basketListener.orderBasket.total()
        let tipValue = total*(Double(Self.tipAmonts[tipAmont])/100)
        return total + tipValue
    }
    
    
    var body: some View {
        Form {
            Section {
               
                Picker(selection: $payementType , label: Text("How do you want to pay")){
                    ForEach (0..<Self.paymentTypes.count) {
                        Text(Self.paymentTypes[$0])
                    }
                }
            }
            Section(header : Text("Add a tip")){
                Picker(selection: $tipAmont, label: Text("How do you want to pay")){
                    ForEach (0..<Self.tipAmonts.count) {
                        Text(String(Self.tipAmonts[$0])+"%")
                    }
                }
            .pickerStyle(SegmentedPickerStyle())
            }
            Section(header: Text("Total : $ \(totalPrice)").font(.largeTitle)){
                Button(action:{
                    self.showingAlert.toggle()
                    self.createOrder()
                    self.emptyBasket()
                    print("Check out")
                }) {
                    Text("Confirm Order")
                }
            }
            .disabled(self.basketListener.orderBasket?.items.isEmpty ?? true)
            
        }
        .navigationBarTitle(Text("Payment"), displayMode: .inline)
        .alert(isPresented: $showingAlert){
            Alert(title: Text("Order Confirmed"), message: Text("Thank You"), dismissButton: .default(Text("OK")))
        }
    }
    private func createOrder(){
        let order =  Order()
        order.amount = totalPrice
        order.id = UUID().uuidString
        order.customerId = FUser.currentId()
        order.orderItems =  self.basketListener.orderBasket.items
        order.saveOrderToFirestore()
    }
    private func emptyBasket(){
        self.basketListener.orderBasket.emptyBasket()
    }
}
 
struct CheckoutView_Previews: PreviewProvider {
    static var previews: some View {
        CheckoutView()
    }
}
