//
//  FirebaseReference.swift
//  Coffee Shope
//
//  Created by piman on 4/25/20.
//  Copyright © 2020 piman. All rights reserved.
//

import Foundation
import FirebaseFirestore

enum FCollectionReference : String{
    case User
    case Menu
    case Order
    case Basket
}

func FirebaseReference(_ collectionReference : FCollectionReference) -> CollectionReference {
    return Firestore.firestore().collection(collectionReference.rawValue)
}
