//
//  Constants.swift
//  Coffee Shope
//
//  Created by piman on 4/25/20.
//  Copyright © 2020 piman. All rights reserved.
//

import Foundation
 
public let userDefaults = UserDefaults.standard
public let KID  = "id"
public let KNAME = "name"
public let KPRICE = "price"
public let KDESCRIPTION = "description"
public let KCATEGORY = "category"
public let KIMAGENAME = "imageName"
 
 
public let KDRINKIDS  = "drinkids"
public let KOWNERID  = "ownerid"
public let KCUSTOMERID  = "customerid"
public let KAMOUNTDS  = "amount"
 
 
public let KEMAIL  = "email"
public let KFIRSTNAME  = "firstname"
public let KLASTNAME = "lastname"
public let KFULLNAME = "fullname"
public let KCURRENTUSER  = "currentUser"
public let KFULLADRESS = "fullAdress"
public let KPHONENUMBER  = "phoneNumber"
public let KONBOARD = "onBoard"
