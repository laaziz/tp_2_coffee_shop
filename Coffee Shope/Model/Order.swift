import Foundation
import Firebase
 
class Order : Identifiable {
    var id : String!
    var customerId : String!
    var orderItems :[Drink] =  []
    var amount : Double!
    
    func saveOrderToFirestore(){
        FirebaseReference(.Order).document(self.id).setData(orderDictionaryFrom(self)) {
            error in
            if error != nil {
                print("Error to save in Firestore " , error!.localizedDescription)
            }
        }
    }
}
 
 
 
func orderDictionaryFrom(_ order : Order) -> [String : Any]{
    var allDrinkIds : [String] =  []
    for drink in order.orderItems {
        allDrinkIds.append(drink.id)
    }
    return NSDictionary(objects: [
                                order.id ,
                                order.customerId ,
                                allDrinkIds ,
                                order.amount
                        ],
                        forKeys: [
                            KID as NSCopying ,
                            KCUSTOMERID as NSCopying ,
                            KDRINKIDS as NSCopying ,
                            KAMOUNTDS as NSCopying]) as!    [String : Any]
}
