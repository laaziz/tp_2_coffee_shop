//
//  Drink.swift
//  Coffee Shope
//
//  Created by piman on 4/25/20.
//  Copyright © 2020 piman. All rights reserved.
//

import Foundation
import Firebase

enum Category : String , CaseIterable , Codable , Hashable{
    case hot
    case cold
    case filter
}
 
struct Drink : Identifiable , Hashable{
    var id : String
    var name : String
    var imageName : String
    var category : Category
    var description : String
    var price : Double
}

func drinkDictionaryFrom(drink : Drink) -> [String:  Any]
{
    return NSDictionary(objects: [drink.id ,
                                  drink.name ,
                                  drink.imageName ,
                                  drink.category.rawValue,
                                  drink.description ,
                                  drink.price
                                 ],
                        forKeys: [ KID as NSCopying ,
                                   KNAME as NSCopying ,
                                   KIMAGENAME as NSCopying ,
                                   KCATEGORY as NSCopying ,
                                   KDESCRIPTION as NSCopying ,
                                   KPRICE as NSCopying
                                ]) as! [String : Any]
}


func createMenu(){
    for drink in drinkData {
        FirebaseReference(.Menu).addDocument(data: drinkDictionaryFrom(drink: drink))
    }
}

let drinkData : [Drink] = []
