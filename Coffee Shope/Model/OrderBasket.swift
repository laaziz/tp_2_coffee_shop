import Foundation
import Firebase

class OrderBasket : Identifiable {
    var id : String!
    var ownerId : String!
    var items :  [Drink] = []
    
    
    func total() -> Double {
        if items.count > 0{
            var s : Double = 0
            for item in items{
                s = s + item.price
            }
            return s
        } else {
            return 0.0
        }
    }
    
    func add (_ item : Drink){
        items.append(item)
    }
    
    func remove(_ item : Drink){
        if let index = items.firstIndex(of: item) {
            items.remove(at: index)
        }
    }
    
    func emptyBasket(){
        self.items =  []
        //save to firebase
    }
    func saveBasketToFirestore(){
        FirebaseReference(.Basket).document(self.id).setData(basketDictionaryFrom(self))
    }
}

func basketDictionaryFrom(_ basket : OrderBasket) ->  [String : Any]{
    var allDrinkIds :  [String] =  []
    for drink in basket.items{
        allDrinkIds.append(drink.id)
    }
    return NSDictionary(objects : [
        basket.id , basket.ownerId , allDrinkIds
        ] ,
                        forKeys:  [KID as NSCopying , KOWNERID as NSCopying , KDRINKIDS as NSCopying]
        
        
        ) as!  [String : Any]
    
}

